﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SandwichMaker
{
    /// <summary>
    /// Interaction logic for CustomDialog.xaml
    /// </summary>
    public partial class CustomDialog : Window
    {

        public event Action<string, string, string> AssignResults;

        public CustomDialog()
        {
            InitializeComponent();
        }

        private void Cmbox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private void BtnCancel_Click(object sender, RoutedEventArgs e)
        {
            //botton cancel either close the window or clear the filled content
            // close the dialog window
            //Close();

            // clear all the filled content
            cmboxBread.Text = "White";
            chkboxLettuce.IsChecked = false;
            chkboxTomatoes.IsChecked = false;
            chkboxCucumbers.IsChecked = false;
            rbtnChicken.IsChecked = false;
            rbtnTurkey.IsChecked = false;
            rbtnBeef.IsChecked = false;
        }

        private void BtnSave_Click(object sender, RoutedEventArgs e)
        {

            string bread = cmboxBread.Text;

            List<string> vegList = new List<string>();
            if (chkboxLettuce.IsChecked == true)
            {
                vegList.Add(chkboxLettuce.Content.ToString());
            }
            if (chkboxTomatoes.IsChecked == true)
            {
                vegList.Add(chkboxTomatoes.Content.ToString());
            }
            if (chkboxCucumbers.IsChecked == true)
            {
                vegList.Add(chkboxCucumbers.Content.ToString());
            }
            string veggies = string.Join(", ", vegList);

            string meat = "";
            if (rbtnChicken.IsChecked == true)
            {
                meat = rbtnChicken.Content.ToString();
            }
            else if (rbtnTurkey.IsChecked == true)
            {
                meat = rbtnTurkey.Content.ToString();
            }
            else if (rbtnBeef.IsChecked == true)
            {
                meat = rbtnBeef.Content.ToString();
            }
            else
            {
                MessageBox.Show("Please choose meat.");
                return;
            }

            AssignResults?.Invoke(bread, veggies, meat);
            DialogResult = true;  // close the dialog window

        }
    }
}
