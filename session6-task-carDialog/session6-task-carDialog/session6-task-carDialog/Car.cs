﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace session6_task_carDialog
{
    public class Car
    {
        public string MakeModel { get; set; }

        public double EngineSize { get; set; }

        public FuelTypeEnum FuelType { get; set; }

        public enum FuelTypeEnum { Gasoline, Diesel, Hybrid, Electric, Other}

        public Car (string m, double e, FuelTypeEnum f)
        {
            this.MakeModel = m;
            this.EngineSize = e;
            this.FuelType = f;            
        }

        public Car() { }
        public override string ToString()
        {
            return $"{MakeModel};{EngineSize};{FuelType}";
        }

        public string ToDataLine()
        {
            return $"{MakeModel};{EngineSize};{FuelType}";
        }

    }
}
