﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace session6_task_carDialog
{
    /// <summary>
    /// Interaction logic for CarDialog.xaml
    /// </summary>
    public partial class CarDialog : Window
    {

        //public event Action<object> AddNewCar;
        public event Action<Car> AddNewCar;
        public Car currCar;

        public CarDialog(Car car)
        {
            InitializeComponent();
            cmbFuelType.ItemsSource = Enum.GetValues(typeof(Car.FuelTypeEnum));
            cmbFuelType.SelectedIndex = 0;

            if (car != null) // to edit the car
            {
                currCar = car;
                txtMake.Text = car.MakeModel;
                sliderEngineSize.Value = car.EngineSize;
                cmbFuelType.SelectedItem = car.FuelType;

                btnSave.Content = "Update";
            }

        }

        private void BtnCancel_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void BtnSave_Click(object sender, RoutedEventArgs e)
        {
            if (txtMake.Text == "")
            {
                MessageBox.Show("Please enter the make and model for your car.");
                return;
            }
            Car.FuelTypeEnum fuelType = (Car.FuelTypeEnum)cmbFuelType.SelectedItem;

            if (currCar != null)
            {
                currCar.MakeModel = txtMake.Text;
                currCar.EngineSize = sliderEngineSize.Value;
                currCar.FuelType = fuelType;
            }
            else
            {
                // Car c = new Car(txtMake.Text, sliderEngineSize.Value, fuelType);
                AddNewCar?.Invoke(new Car(txtMake.Text, sliderEngineSize.Value, fuelType));
            }

            DialogResult = true;
        }
    }
}
