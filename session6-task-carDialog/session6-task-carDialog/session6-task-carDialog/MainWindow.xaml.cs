﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using CsvHelper;
using System.Globalization;

namespace session6_task_carDialog
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        List<Car> carList = new List<Car>();
        public string currentFileName;
        public string currentFilePath;

        private void MiOpenfILE_Click(object sender, RoutedEventArgs e)
        {
            //????????????????? check if there is unsaved file change???
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.DefaultExt = ".txt";
            openFileDialog.Filter = "Text files (*.txt)|*.txt|All files (*.*)|*.*";
            if (openFileDialog.ShowDialog() == true)
            {
                currentFileName = openFileDialog.FileName;
                ReadDataFromFile(currentFileName);
                lvGridCar.ItemsSource = carList;
                RefreshStatusBar();
            }
        }

        private void MiSave_Click(object sender, RoutedEventArgs e)
        {
            SaveFile(currentFileName);

        }

        private void MiSaveFile_Click(object sender, RoutedEventArgs e)
        {
            // save content into a new file
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.Filter = "Text files (*.txt)|*.txt|All files (*.*)|*.*";
            saveFileDialog.Title = "Save all the cars in a file";
            if (saveFileDialog.ShowDialog() == true)
            {
                SaveFile(saveFileDialog.FileName);
            }
            else
            {
                return;
            }
        }

        private void miExportCVS_Click(object sender, RoutedEventArgs e)
        {
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.Filter = "CSV files (*.csv)|*.csv";
            saveFileDialog.Title = "Export to file";

            if (saveFileDialog.ShowDialog() == true)
            {
                try
                {
                    using (StreamWriter sw = new StreamWriter(saveFileDialog.FileName))
                    {
                        using (CsvWriter cw = new CsvWriter(sw, CultureInfo.InvariantCulture))
                        {
                            cw.Configuration.Delimiter = ",";  //***************************
                            cw.WriteRecords(carList);
                        }

                    }
                }
                catch (IOException exc)
                {
                    MessageBox.Show("Error: " + exc.Message);
                }
            }

        }

        private void MiExit_Click(object sender, RoutedEventArgs e)
        {
            //????????????????? check if there is unsaved file change???
            var result = MessageBox.Show("Do you want to exit", "Warning", MessageBoxButton.YesNo, MessageBoxImage.Warning);
            switch (result)
            {
                case MessageBoxResult.Yes:
                    Close();
                    break;
                case MessageBoxResult.No:
                    return;
            }
        }

        private void MiAdd_Click(object sender, RoutedEventArgs e)
        {
            lvGridCar.ItemsSource = carList;
            CarDialog carDialog = new CarDialog(null);
            carDialog.Owner = this;

            carDialog.AddNewCar += (c) => { carList.Add(c); };
            bool? result = carDialog.ShowDialog();

            RefreshLvWindow();
        }

        private void MenuEdit_Click(object sender, RoutedEventArgs e)
        {
            if (lvGridCar.SelectedIndex == -1)
            {
                return;
            }

            Car carToEdit = (Car)lvGridCar.SelectedItem;
            CarDialog carDialog = new CarDialog(carToEdit);
            carDialog.Owner = this;

            bool? result = carDialog.ShowDialog();
            if (result == true)
            {
                RefreshLvWindow();
            }
        }


        private void MiDelete_Click(object sender, RoutedEventArgs e)
        {
            if (lvGridCar.ItemsSource == null)
            {
                MessageBox.Show("There is no file.");
                return;
            }

            if (lvGridCar.SelectedIndex == -1)
            {
                MessageBox.Show("Please select a car.");
                return;
            }
            Car carToDelete = (Car)lvGridCar.SelectedItem;
            if (carToDelete != null)
            {
                MessageBoxResult result = MessageBox.Show("Are you sure to delete this car?", "Confirmation", MessageBoxButton.YesNo, MessageBoxImage.Warning);
                if (result == MessageBoxResult.Yes)
                {
                    carList.Remove(carToDelete);
                }
            }
            RefreshLvWindow();

        }


        private void ReadDataFromFile(string fileName)
        {
            if (File.Exists(fileName))
            {
                string[] carLines = File.ReadAllLines(fileName);
                foreach (string carLine in carLines)
                {
                    string[] carInfo = carLine.Split(';');

                    Car car = new Car();

                    car.MakeModel = carInfo[0];

                    double engineSize;
                    if (!double.TryParse(carInfo[1], out engineSize))
                    {
                        MessageBox.Show("Error: Invalid value for Engine Size: " + carLine);
                    }
                    else if (engineSize < 1.0 || engineSize > 20.0)
                    {
                        MessageBox.Show("Error: Value for Engine Size out of range: " + carLine);
                    }

                    Car.FuelTypeEnum fuelType;
                    if (!Enum.TryParse(carInfo[2], out fuelType))
                    {
                        MessageBox.Show("Error: Invalid value for Fuel Type: " + carLine);
                    }

                    carList.Add(new Car(carInfo[0], engineSize, fuelType));
                }
            }
        }

        private void RefreshLvWindow()
        {
            lvGridCar.SelectedIndex = -1;
            lvGridCar.Items.Refresh();
            RefreshStatusBar();
        }

        private void RefreshStatusBar()
        {
            tbStatus.Text = $"You have {carList.Count} car(s) currently";

        }

       

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            //????????????????? check if there is unsaved file change???
        }


        private void SaveFile(string fileName)
        {
            // write content into the original file
            try
            {
                using (StreamWriter sw = new StreamWriter(fileName))
                {
                    foreach (Car c in carList)
                    {
                        sw.WriteLine(c.ToDataLine());
                    }
                }
            }
            catch (IOException exc)
            {
                MessageBox.Show(exc.Message);
            }
        }



    }
}
