﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace HelloTwoWindows
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {        
        public MainWindow()
        {
            InitializeComponent();
        }

        private void btnHello_Click(object sender, RoutedEventArgs e)
        {
            string name = txtName.Text;
            int age = 0;
            string city = "";

            HelloDialog helloWindow = new HelloDialog(name); // call constructor of HelloWindow
            //helloWindow.Show();

            helloWindow.Owner = this; // this line opens the child in the same positino as parent
            
            //**********
            // first assign ... to AssignResult
            // then open the dialog ShowDialog
            helloWindow.AssignResult += (a, c) => { age = a; city = c; };
            bool? result = helloWindow.ShowDialog();
            
            if (result == true)
            {
                lblResult.Content = $"Age: {age}, City: {city}";
            }

        }
    }
}
