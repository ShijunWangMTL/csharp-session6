﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace HelloTwoWindows
{
    /// <summary>
    /// Interaction logic for HelloWindow.xaml
    /// </summary>
    public partial class HelloDialog : Window
    {

        public event Action<int, string> AssignResult; //an event is an encapsulated delegate

        public HelloDialog(string name)
        {
            InitializeComponent();
            lblMsg.Content = $"Hello {Name}, nice to meet you!";
        }

        private void btnOk_Click(object sender, RoutedEventArgs e)
        {
            int age = int.Parse(txtBoxAge.Text);
            string city = txtCity.Text;

            AssignResult?.Invoke(age, city);
            DialogResult = true;
        }
    }
}
