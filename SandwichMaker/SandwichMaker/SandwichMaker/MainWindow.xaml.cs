﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SandwichMaker
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void BtnMkSandwich_Click(object sender, RoutedEventArgs e)
        {
            /*string bread = "";
            string veggies = "";
            string meat = "";*/

            CustomDialog customDialog = new CustomDialog();

            // customDialog.Owner = this;

            // delegate invoke a method
            customDialog.AssignResults += CustomDialog_AssignResult;

            //lambda expression avoid to create another method to call the parameters
            //customDialog.AssignResults += (a, b, c) => { bread = a; veggies = b; meat = c; };

            // or
            customDialog.AssignResults += (a, b, c) => { lblResultBread.Content = a; lblResultVeg.Content = b; lblResultMeat.Content = c; };

            bool? result = customDialog.ShowDialog();

        }

        private void CustomDialog_AssignResult(string b, string v, string m)
        {
            lblResultBread.Content = b;
            lblResultVeg.Content = v;
            lblResultMeat.Content = m;
        }


    }
}
